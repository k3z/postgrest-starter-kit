NOW = $(sh date +%Y%m)

default: here


here:
	@sh -c "pwd"
	@echo "---"
	@echo "Access Swagger http://$(SWAGGER_PUBLIC_URL)"
	@echo "---"

init-db:
	@docker exec service-db sh -c "rm -rf /tmp/sql/"
	@docker cp db/src/ service-db:/tmp/sql/
	@docker-compose exec db sh -c "cd /tmp/sql/ && psql --quiet -U \${POSTGRES_USER} \${POSTGRES_DB} -f /tmp/sql/init.sql"
