# Service App

## Run Tests

### Run all tests

	$ docker-compose exec api pytest ./tests -v -s

### Run specific module tests

	$ docker-compose exec api pytest ./tests/test_hello.py -v -s

### Run specific module method tests

	$ docker-compose exec api pytest ./tests/test_hello.py::test_api_version -v -s


