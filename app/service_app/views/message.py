from flask import current_app as app
from flask import Blueprint
from flask import abort


bp_message = Blueprint(
    'messages',
    __name__,
    template_folder='templates',
    static_folder='static',
    static_url_path='/static',
    url_prefix='/messages',
)


@bp_message.app_template_filter('static_url')
def static_url(resource):
    return '%s%s' % (app.config.get('BLAST_STATIC_URL'), resource)


# @bp_message.route('/recipients/<recipient_id>/')
# def preview_message(recipient_id):
#     m = MessageRecipient.query.get(recipient_id)
#     if m:
#         return m.render_message()
#     abort(404)


# @bp_message.route('/unsubscribe/<recipent_id>/')
# def unsubscribe(recipent_id):
#     mr = MessageRecipient.query.get(recipent_id)
#     if mr:
#         mr.unsubscribe()
#         return (
#             'Désolé de vous voir partir, votre email a été retiré de la liste.'
#         )
#     abort(404)
