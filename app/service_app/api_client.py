import sys
import logging
from datetime import datetime

from box import Box

import jwt
from requests import request

logger = logging.getLogger('apiclient')
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(sys.stderr))


class Client(object):
    def __init__(
        self,
        app=None,
        host=None,
        endpoint='/api/1/',
        auth_endpoint='/auth',
        debug=False,
        autologin=False,
    ):
        if debug:
            self.level = logging.INFO
        else:
            self.level = logging.DEBUG

        self.app = app
        if app is not None:
            self.init_app(app)

        self.root = host
        self.username = None
        self.password = None
        self.endpoint = endpoint

        self.last_request = None

    def init_app(self, app):
        self.root = app.config.get('SERVICE_API_HOST', self.root)
        self.username = app.config.get('SERVICE_API_LOGIN', self.login)
        self.password = app.config.get('SERVICE_API_PASSWORD', self.password)

    def call(
        self,
        url,
        method='GET',
        params=None,
        headers=None,
        files=None,
        json=None,
        cookies={},
        force_auth=False,
        mask=None,
    ):
        if headers is None:
            headers = {'user-agent': 'client-python/1.0'}
        else:
            headers['user-agent'] = 'client-python/1.0'

        if self.access_token:
            headers['Authorization'] = 'JWT %s' % self.access_token

        if mask:
            headers['X-Fields'] = mask

        r = request(
            method,
            '%s%s%s'
            % (self.root, ('' if force_auth is True else self.endpoint), url),
            data=params,
            cookies=cookies,
            files=files,
            json=json,
            headers=headers,
        )

        if r.status_code == 502:
            self.log('Unable to connect to %s' % (r.url))
            # raise ValueError('Unable to connect to %s' % self.root)

        return Response(self, r, method)

    def login(self, username=None, password=None):
        params = {
            'email': username if username else self.username,
            'password': password if password else self.password,
        }
        r = self.call('POST', self.auth_endpoint, json=params, force_auth=True)
        if r.response().headers.get('Authorization'):
            token = r.response().headers.get('Authorization')
            if token.startswith('JWT '):
                self.access_token = token.split('JWT ')[1]
        return r

    def logout(self):
        r = self.call('DELETE', 'auth/', {})
        self.access_token = None
        return r

    def set_session(self, token):
        self.access_token = token

    def get_session(self):
        return self.access_token

    def log(self, *args, **kwargs):
        """Proxy access to the politis api logger,
        changing the level based on the debug setting"""
        logger.log(self.level, *args, **kwargs)

    def __repr__(self):
        return '<YesYesAPIRequest root="%s">' % self.root


class Response(object):
    def __init__(self, master, response, method):
        self.__master = master
        self.__response = response
        self.__method = method
        self.__json = None
        self.__box = None

    def response(self):
        return self.__response

    def raw(self):
        return self.__response.text

    def status_code(self):
        return self.__response.status_code

    def session(self):
        return self.__master.access_token

    def valid(self):
        if self.__response.status_code == 200:
            return True

    def is_valid(self):
        return self.valid()

    def pagination(self):
        return {
            'page': int(self.__response.headers.get('X-Pagination-Page', 0)),
            'per_page': int(
                self.__response.headers.get('X-Pagination-Per_Page', 0)
            ),
            'has_next': True
            if self.__response.headers.get('X-Pagination-Has_Next') == 'True'
            else False,
            'has_prev': True
            if self.__response.headers.get('X-Pagination-Has_Prev') == 'True'
            else False,
            'pages': int(self.__response.headers.get('X-Pagination-Pages', 0)),
            'total': int(self.__response.headers.get('X-Pagination-Total', 0)),
        }

    def boxed(self):
        if self.__response.status_code == 200:
            if not self.__box:
                self.__box = Box(self.__response.json())
            return self.__box

    def data(self):
        if self.__response.status_code == 200:
            if not self.__json:
                self.__json = self.__response.json()
            return self.__json

    def __getattr__(self, attr):
        return self.data().get(attr)

    def __repr__(self):
        return '<Response url="{}" method="{}" status="{}" time="{}">'.format(
            self.__response.url,
            self.__method,
            self.__response.status_code,
            self.__response.elapsed,
        )
