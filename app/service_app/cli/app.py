""" Master CLI commands """
import click  # noqa

from flask import Blueprint


bp = Blueprint('cli', __name__, cli_group='blast')


@bp.cli.command()
def hello():
    """ Hello """
    print(hello)
