import os
import json
import pprint
import datetime
from uuid import UUID
from itsdangerous import URLSafeTimedSerializer

pp = pprint.PrettyPrinter(width=120, compact=True, sort_dicts=False)


class Console(object):
    def __init__(self, debug=False):
        self.debug = debug

    def print(self, data):
        if self.debug is True:
            print(data)

    def pprint(self, data, delimiter=None):
        if self.debug is True:
            if delimiter:
                print(delimiter)
            pp.pprint(data)
            if delimiter:
                print(delimiter)


def is_uuid(uuid_string, version=4):
    try:
        uid = UUID(uuid_string, version=version)
        return uid.hex == uuid_string.replace('-', '')
    except ValueError:
        return False


def jsonSerializer(o):
    if isinstance(o, (datetime.date, datetime.datetime)):
        return o.isoformat()


def dict_jsonify(data):
    dump = json.dumps(data, sort_keys=True, indent=1, default=jsonSerializer)
    return json.loads(dump)


def filter_dict_with_keys(data, keys, allowed_keys=[]):
    results = {}
    data_keys = data.keys()
    for key in keys:
        key = key.strip()
        if key not in data_keys:
            continue
        if allowed_keys and key not in allowed_keys:
            continue
        value = data.get(key)
        results[key] = value
    return results


def env_var(key, default=None):
    val = os.environ.get(key, default)
    if val == 'True':
        val = True
    elif val == 'False':
        val = False
    return val


def boolean(val):
    if val.lower() == 'true':
        return True
    if val.lower() == 'false':
        return False
    return val


def truncate(content, length=100, suffix=u'…'):
    if content:
        if len(content) <= length:
            return content
        else:
            return (
                ' '.join(content[: length + 1 - len(suffix)].split(' ')[0:-1])
                + suffix
            )


def allowed_file(filename, allowed_extensions=['jpg', 'jpeg', 'png']):
    return (
        '.' in filename
        and filename.rsplit('.', 1)[1].lower() in allowed_extensions
    )


def generate_token(secret, payload):
    s = URLSafeTimedSerializer(secret)
    return s.dumps({'payload': payload})


def decode_token(secret, token, expiration=3600):
    s = URLSafeTimedSerializer(secret)
    payload = None
    valid = False
    error = None
    forged_at = None
    expires_at = None
    try:
        decoded = s.loads(token, return_timestamp=True)
        payload = decoded[0]['payload']
        now = datetime.datetime.utcnow()
        forged_at = decoded[1]
        expires_at = forged_at + datetime.timedelta(seconds=expiration)
        if expires_at >= now:
            valid = True
    except Exception as e:
        error = str(e)
    return {
        'payload': payload,
        'valid': valid,
        'forged_at': str(forged_at),
        'expires_at': str(expires_at),
        'error': error,
    }
