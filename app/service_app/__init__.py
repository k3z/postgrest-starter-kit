from service_app.app import AppFactory


def create_app():
    return AppFactory()
