from flask import current_app as app

from flask_mail import Mail
from flask_mail import Message

mail = Mail()


def send_email(
    subject,
    sender,
    recipients,
    text_body,
    html_body,
    reply_to=None,
    user=None,
    preview_url=None,
    headers=None,
    log=False,
):
    msg = Message(
        subject, sender=sender, recipients=recipients, reply_to=reply_to
    )
    msg.body = text_body
    msg.html = html_body
    msg.extra_headers = headers

    try:
        mail.send(msg)
        send = True
    except Exception as e:
        app.logger.error(e)
        send = False
    return send
