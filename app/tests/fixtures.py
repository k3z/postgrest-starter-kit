def reset_fixtures():
    pass


USER0 = {
    'id': 'd6643317-a745-4dfc-8cc9-71d8da9c5e69',
    'email': 'john@doe.com',
    'status': 'ENABLED',
}

USER1 = {
    'id': '5a8a2eb2-a165-4845-a897-ef5e5eae4e0f',
    'status': 'ENABLED',
    'email': 'jane@doe.com',
    'password': 'supersecret',
    'firstname': 'Jane',
    'lastname': 'Doe',
    'entity': None,
    'username': 'JD',
    'screenname': 'Jane',
    'birthday': '2000-01-01',
    'gender': 'F',
    'phone': '0644335566',
}
