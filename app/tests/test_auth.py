import pytest  # noqa
import requests  # noqa
from pprint import pprint  # noqa
from service_app.config import SERVICE_API_URL


def test_login(client, app):
    HEADERS = {'Accept': 'application/vnd.pgrst.object+json'}
    r = requests.post(
        '{}/rpc/login'.format(SERVICE_API_URL),
        data={'email': 'alice@email.com', 'password': 'pass'},
        headers=HEADERS,
    )
    assert r.status_code == 200


def test_me(client, app):
    HEADERS = {'Accept': 'application/vnd.pgrst.object+json'}
    r = requests.post(
        '{}/rpc/login'.format(SERVICE_API_URL),
        data={'email': 'alice@email.com', 'password': 'pass'},
        headers=HEADERS,
    )
    data = r.json()
    headers = {'Authorization': 'Bearer {}'.format(data['login']['token'])}
    r = requests.post('{}/rpc/me'.format(SERVICE_API_URL), headers=headers)
    assert r.status_code == 200


def test_refresh_token(client, app):
    HEADERS = {'Accept': 'application/vnd.pgrst.object+json'}
    r = requests.post(
        '{}/rpc/login'.format(SERVICE_API_URL),
        data={'email': 'alice@email.com', 'password': 'pass'},
        headers=HEADERS,
    )
    data = r.json()
    headers = {'Authorization': 'Bearer {}'.format(data['login']['token'])}
    r = requests.post(
        '{}/rpc/refresh_token'.format(SERVICE_API_URL), headers=headers,
    )
    assert r.status_code == 200
