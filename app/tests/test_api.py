import pytest  # noqa
import requests  # noqa
from pprint import pprint
from .fixtures import reset_fixtures
from service_app.config import SERVICE_API_URL


@pytest.fixture(autouse=True)
def run_around_tests(app):
    # before test
    reset_fixtures()

    yield

    # after test
    reset_fixtures()


def test_list(client, app):
    HEADERS = {'Accept': 'application/vnd.pgrst.object+json'}
    r = requests.post(
        '{}/rpc/login'.format(SERVICE_API_URL),
        data={'email': 'alice@email.com', 'password': 'pass'},
        headers=HEADERS,
    )
    data = r.json()
    headers = {
        **{'Authorization': 'Bearer {}'.format(data['login']['token'])},
        # **HEADERS,
    }
    # params = {'select': 'title,author_id(*)'}
    params = {}
    print(params)
    r = requests.get(
        '{}/stories'.format(SERVICE_API_URL), params=params, headers=headers,
    )
    assert r.status_code == 200
    print(r.text)
    # pprint(r.json())
