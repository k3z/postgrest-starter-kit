import pytest  # noqa
import requests  # noqa
from .fixtures import reset_fixtures
from service_app.config import SERVICE_API_URL


@pytest.fixture(autouse=True)
def run_around_tests(app):
    # before test
    reset_fixtures()

    yield

    # after test
    reset_fixtures()


def test_api_version(client, app):
    r = requests.get('{}/'.format(SERVICE_API_URL))
    print(r.status_code)
    assert r.status_code == 200
