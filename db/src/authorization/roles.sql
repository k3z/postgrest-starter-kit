\echo # Loading roles

-- this is an application level role
-- requests that are not authenticated will be executed with this role's privileges
drop role if exists anonymous;
create role anonymous nologin;
grant anonymous to admin;

-- role for the main application user accessing the api
drop role if exists webuser;
create role webuser nologin;
grant webuser to admin;
