\echo # Loading roles privilege

grant usage on schema api to anonymous, webuser;

grant execute on function api.login(text,text) to anonymous;
grant execute on function api.login(text,text) to webuser;
grant execute on function api.me() to webuser;
grant execute on function api.refresh_token() to webuser;

grant select, insert, update, delete on data.author to api;
grant select on api.authors to webuser;

grant select, insert, update, delete on data.story to api;
grant select on api.stories to webuser;
