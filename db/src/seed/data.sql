\echo # filling table data.user
INSERT INTO data.user ( id,name,email,"password" )
VALUES
	('42a8eecd-26a9-41c1-855c-200535ec9ad9', 'alice', 'alice@email.com', 'pass'),
	('a22f6faf-a8f9-44c8-b23c-f038cba60008', 'bob', 'bob@email.com', 'pass');


\echo # filling table data.author
INSERT INTO data.author ( id, firstname, lastname )
VALUES
	('6d3c12a1-8a3f-4056-ab02-e1d73b7012ae', 'Carine', 'Fouteau'),
	('880d8363-c421-433f-a29d-cf8f6eabfff5', 'Mathilde', 'Mathieu');

ANALYZE data.author;


\echo # filling table data.story
INSERT INTO data.story ( id, title, author_id )
VALUES
	('2adef258-d393-4324-ac1f-19ab8c591a32', 'A Madrid, les quartiers populaires s’organisent face au Covid', '6d3c12a1-8a3f-4056-ab02-e1d73b7012ae'),
	('72e057ea-9162-4ca6-999d-bb1b008c1b08', 'A gauche, l’obsession républicaine nourrit la discorde', '880d8363-c421-433f-a29d-cf8f6eabfff5');

ANALYZE data.story;


