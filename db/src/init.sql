\set QUIET on
\set ON_ERROR_STOP on
set client_min_messages to warning;

\set jwt_secret `echo $APP_SECRET`
\set quoted_jwt_secret '\'' :jwt_secret '\''

\echo # Loading database definition

\ir libs/settings.sql

select settings.set('jwt_secret', :quoted_jwt_secret);
select settings.set('jwt_lifetime', '3600');

\ir libs/request.sql

\ir data/schema.sql
\ir api/schema.sql

\ir authorization/roles.sql
\ir authorization/privileges.sql

\ir seed/data.sql
