create table author (
	id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
	created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	deleted boolean default false,
	firstname VARCHAR,
	lastname VARCHAR
);
