create table story (
	id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
	created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	deleted boolean default false,
	status VARCHAR NOT NULL DEFAULT 'ENABLED',
	title VARCHAR NOT NULL,
	author_id uuid NOT NULL REFERENCES author (id)
);
