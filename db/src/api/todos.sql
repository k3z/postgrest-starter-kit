create or replace view todos as
select id, todo, private, (owner_id = request.user_id()) as mine from data.todo;
alter view todos owner to api; -- it is important to set the correct owner to the RLS policy kicks in
